# Aina-Bulaque STM32H723 Custom board

Developed with KiCad 7.0.10

Features:
- Ethernet
- SDRAM with W9825G6KH SDRAM chip

[<img src="board-screenshot.png" alt="Board Screenshot" width="200"/>](https://gitlab.com/jasuramme/ab-stm32h723/-/raw/main/board-screenshot.png?inline=false)


## Why this board looks so strange?

It is part of my personal private project. I've shared what I can.

## Is it tested?

Yes, this design was produced with JLCPCB shop and works fine. Please see related software repos and production instructions below.

## I found a problem in design, what do I do?

This is more like a draft for my personal project, so there are many places for improvement. Please feel free to create PR with verbose description or conact me. 

## How do I produce it?

This design was tested and produced with [JLCPCB](https://jlcpcb.com/). Stackup used: JLC04161H-7628, 4 layers with 1x7628 prepeg between 1-2 and 3-4 layers.

## Other repos:

- LWIP full TCP-IP stack example: [https://gitlab.com/jasuramme/custom_stmh723_eth](https://gitlab.com/jasuramme/custom_stmh723_eth)
- Low level example code with SDRAM and Ethernet Layer-2 example [https://gitlab.com/jasuramme/ab-stm32h7-lowlevel-example](https://gitlab.com/jasuramme/ab-stm32h7-lowlevel-example)